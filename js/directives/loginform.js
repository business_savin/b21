angular.module('MyApp').directive('loginform', [function () {

    var parentEl = angular.element(document.body);

    return {
        restrict: 'E',
        scope: {
            loginform: '='
        },
        templateUrl: '/js/templates/form.html',
        replace: 'true',
        controller: ['$scope', '$rootScope', '$location', function ($scope, $rootScope, $location) {

                $scope.name = $rootScope.users.username;
                $scope.paswd = $rootScope.users.userpassword;

                $scope.submit = function () {

                    $rootScope.users.name = $scope.name;
                    $rootScope.users.username = $scope.login;
                    $rootScope.users.userpassword = $scope.paswd;


                    if ($rootScope.users.username === 'admin' && $rootScope.users.userpassword === 'admin' && $rootScope.users.name != "") {
                        $rootScope.users.loggetIn = true;
                        $location.path('/auth');
                    } else {
                        $rootScope.showAlert('Неверный логин или пароль')
                    }
                }
                $scope.notfunc = function () {
                    $rootScope.showAlert('Функционал не реализован');
                }
            }
          ]
    };
}]);