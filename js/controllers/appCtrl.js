angular.module('MyApp').controller('AppCtrl', ['$scope', 'localStorageService', '$rootScope', '$mdDialog', '$mdMedia', function ($scope, localStorageService, $rootScope, $mdDialog, $mdMedia) {
    $rootScope.users = {
        'name': '',
        'username': '',
        'userpassword': '',
        'loggetIn': false
    }
    $rootScope.showAlert = function (texted) {
        alert = $mdDialog.alert({
            title: 'ВНИМАНИЕ!',
            textContent: texted,
            ok: 'Закрыть'
        });
        $mdDialog
            .show(alert)
            .finally(function () {
                alert = undefined;
            });
    }

    $scope.showAdvanced = function ($event) {

        $mdDialog.show({
                targetEvent: $event,
                scope: $scope,
                preserveScope: true,
                controller: DialogController,
                templateUrl: '/js/templates/dialog.html',
                parent: angular.element(document.body),
                clickOutsideToClose: true,
                fullscreen: true,
                //                locals: { name: $rootScope.users.username; } 
            })
            .then(function (answer) {
                $scope.status = 'Вы ответили "' + answer + '".';
            }, function () {
                $scope.status = 'Вы закрыли окно.';
            });

        function DialogController() {
            $scope.hide = function () {
                $mdDialog.hide();
            };
            $scope.cancel = function () {
                $mdDialog.cancel();
            };
            $scope.answer = function (answer) {
                $mdDialog.hide(answer);
            };
        }

    };

}]);