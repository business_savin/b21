angular.module('MyApp')

.controller('pathsController', ['$scope', '$rootScope', function ($scope, $rootScope) {
    $scope.links = [
        {
            link: "/",
            name: "Главная"
        },
        {
            link: "/login",
            name: "Вход"
        },
        {
            link: "/auth",
            name: "Секрет"
        }
    ];

}])

.controller('mainpageController', ['$scope', '$routeParams', function ($scope, $routeParams) {
    $scope.name = "page1Controller";
    $scope.params = $routeParams;
 }])

.controller('page2Controller', ['$scope', '$routeParams', function ($scope, $routeParams) {
    $scope.name = "page2Controller";
    $scope.params = $routeParams;
 }])

.controller('tmp_mainController', ['$scope', '$routeParams', function ($scope, $routeParams) {
    $scope.name = "page3Controller";
    $scope.params = $routeParams;
 }])


.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
    $routeProvider
        .when('/', {
            templateUrl: 'mainpage.html',
            controller: 'mainpageController'
        })
        .when('/login', {
            templateUrl: 'page2.html',
            controller: 'page2Controller'
        })
        .when('/auth', {
            resolve: {
                "check": ['$location', '$mdDialog', '$rootScope', function ($location, $mdDialog, $rootScope) {

                    if ($rootScope.users.username == 'admin' && $rootScope.users.userpassword == 'admin') {
                        console.log($rootScope.users.username, $rootScope.users.userpassword);
                    } else {
                        $rootScope.showAlert('Введите пароль!');
                        $location.path('/');
                    }

                    }]
            },
            controller: 'tmp_mainController',
            templateUrl: 'tmp_main.html'
        });
    $routeProvider.otherwise({
        redirectTo: '/'
    });

    // configure html5 to get links working on jsfiddle
    $locationProvider.html5Mode(true);

}]);