var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync');
var uglify = require('gulp-uglify'); // Минификация JS
var concat = require('gulp-concat'); // Склейка файлов

//Склейка js файлов
gulp.task('scripts', function () {
    return gulp.src([
        'js/modules/*.js',
        'js/controllers/*.js',
        'js/controllers/*.js',
        'js/directives/*.js',
        'js/services/*.js'
    ]) //сборка в указанном порядке
        .pipe(concat('app.js')) //клеим
        .pipe(uglify('app.js')) //сжимаем
        .pipe(gulp.dest('js/')); //выкладываем
});

//Конвертируем scss в css и объеденям
gulp.task('sass', function() {
  return gulp.src('css/style.scss')
    .pipe(sass())
    .pipe(gulp.dest('css/'))
    .pipe(browserSync.reload({
      stream: true
    }))
});

//Создаем виртуальный сервер для прменения изменений сразу
gulp.task('browserSync', function () {
    browserSync({
        server: {
            baseDir: '/web/b21/' //укажите свой path
        },
    })
})

//Следим за изменениями автоматически
gulp.task('watch', ['browserSync', 'scripts', 'sass'], function () {
    gulp.watch('css/*.scss', ['sass']);
    gulp.watch('js/**/*.js', ['scripts']);
    // Обновляем браузер при любых изменениях в HTML или JS
    gulp.watch('*.html', browserSync.reload);
    gulp.watch('js/**/*.js', browserSync.reload);
});

//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
//@@@@@@@@@@@ для продакш @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
//js
gulp.task('produtionsjs', function () {
    return gulp.src([
        'bower_components/jquery/dist/jquery.js',
        'bower_components/angular/angular.js',
        'bower_components/angular-messages/angular-messages.js',
        'bower_components/angular-material/angular-material.js',
        'bower_components/angular-aria/angular-aria.js',
        'bower_components/angular-animate/angular-animate.js',
        'bower_components/angular-bootstrap/ui-bootstrap.js',
        'bower_components/angular-route/angular-route.js',
        'bower_components/angular-local-storage/dist/angular-local-storage.js',
        'js/modules/*.js',
        'js/controllers/*.js',
        'js/controllers/*.js',
        'js/directives/*.js',
        'js/services/*.js'
    ]) //сборка в указанном порядке
        .pipe(concat('app.js')) //клеим
        .pipe(uglify('app.js')) //сжимаем
        .pipe(gulp.dest('js/')); //выкладываем
});
